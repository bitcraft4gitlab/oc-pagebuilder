<?php namespace Bitcraft\Pagebuilder\Models;

use Model;
use RainLab\Translate\Models\Locale;

/**
 * Model
 */
class Menu extends Model
{
    use \October\Rain\Database\Traits\Validation;


    /**
     * @var string The database table used by the model.
     */
    public $table = 'bitcraft_pagebuilder_menus';
    public $implement = ['RainLab.Translate.Behaviors.TranslatableModel'];
    protected $jsonable = ['items'];
    public $translatable = [
        'items',
        'startpage_link',
        'cta_title',
        'cta_url'
    ];

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public function hasItems()
    {
        if (!empty($this->items)) {
            return true;
        }

        foreach (array_keys(Locale::listAvailable()) as $locale) {
            if (!empty($this->getAttributeTranslated('items', $locale))) {
                return true;
            }
        }
        return false;
    }
}
