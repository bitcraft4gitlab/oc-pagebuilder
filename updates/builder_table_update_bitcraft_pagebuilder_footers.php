<?php namespace Bitcraft\Pagebuilder\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBitcraftPagebuilderFooters extends Migration
{
    public function up()
    {
        Schema::table('bitcraft_pagebuilder_footers', function($table)
        {
            $table->string('title');
        });
    }

    public function down()
    {
        Schema::table('bitcraft_pagebuilder_footers', function($table)
        {
            $table->dropColumn('title');
        });
    }
}
