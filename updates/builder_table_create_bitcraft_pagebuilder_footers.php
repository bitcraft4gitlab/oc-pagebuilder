<?php namespace Bitcraft\Pagebuilder\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateBitcraftPagebuilderFooters extends Migration
{
    public function up()
    {
        Schema::create('bitcraft_pagebuilder_footers', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->string('image')->nullable();
            $table->text('image_alt')->nullable();
            $table->string('image_title');
            $table->text('social')->nullable();
        });
    }

    public function down()
    {
        Schema::dropIfExists('bitcraft_pagebuilder_footers');
    }
}
