<?php namespace Bitcraft\Pagebuilder\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBitcraftPagebuilderPages3 extends Migration
{
    public function up()
    {
        Schema::table('bitcraft_pagebuilder_pages', function($table)
        {
            $table->integer('platform_id')->unsigned()->default(1);
        });
    }

    public function down()
    {
        Schema::table('bitcraft_pagebuilder_pages', function($table)
        {
            $table->dropColumn('platform_id');
        });
    }
}
