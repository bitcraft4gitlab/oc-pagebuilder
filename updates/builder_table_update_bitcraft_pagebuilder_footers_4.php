<?php namespace Bitcraft\Pagebuilder\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBitcraftPagebuilderFooters4 extends Migration
{
    public function up()
    {
        Schema::table('bitcraft_pagebuilder_footers', function($table)
        {
            $table->boolean('show_newsletter')->nullable();
        });
    }

    public function down()
    {
        Schema::table('bitcraft_pagebuilder_footers', function($table)
        {
            $table->dropColumn('show_newsletter');
        });
    }
}
