<?php namespace Bitcraft\Pagebuilder\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateBitcraftPagebuilderPages extends Migration
{
    public function up()
    {
        Schema::create('bitcraft_pagebuilder_pages', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->string('title');
            $table->text('modules')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->integer('menu_id')->unsigned();
            $table->boolean('published')->default(0);
        });
    }

    public function down()
    {
        Schema::dropIfExists('bitcraft_pagebuilder_pages');
    }
}
