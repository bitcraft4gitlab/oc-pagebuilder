<?php namespace Bitcraft\Pagebuilder\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBitcraftPagebuilderPages2 extends Migration
{
    public function up()
    {
        Schema::table('bitcraft_pagebuilder_pages', function($table)
        {
            $table->string('slug')->nullable();
        });
    }

    public function down()
    {
        Schema::table('bitcraft_pagebuilder_pages', function($table)
        {
            $table->dropColumn('slug');
        });
    }
}
