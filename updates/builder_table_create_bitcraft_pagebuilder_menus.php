<?php namespace Bitcraft\Pagebuilder\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateBitcraftPagebuilderMenus extends Migration
{
    public function up()
    {
        Schema::create('bitcraft_pagebuilder_menus', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->string('menu_title');
            $table->text('items')->nullable();
            $table->string('logo')->nullable();
        });
    }

    public function down()
    {
        Schema::dropIfExists('bitcraft_pagebuilder_menus');
    }
}
