<?php namespace Bitcraft\Pagebuilder\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBitcraftPagebuilderMenus2 extends Migration
{
    public function up()
    {
        Schema::table('bitcraft_pagebuilder_menus', function($table)
        {
            $table->string('cta_title')->nullable();
            $table->string('cta_url')->nullable();
        });
    }

    public function down()
    {
        Schema::table('bitcraft_pagebuilder_menus', function($table)
        {
            $table->dropColumn('cta_title');
            $table->dropColumn('cta_url');
        });
    }
}
